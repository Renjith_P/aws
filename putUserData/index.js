'use strict'
const AWS = require('aws-sdk');

AWS.config.update({ region: "ap-south-1"});

exports.handler = async (event, context) => {
  const ddb = new AWS.DynamoDB({ apiVersion: "2012-10-08"});
  const documentClient = new AWS.DynamoDB.DocumentClient({ region: "ap-south-1"});

  const params = {
    TableName: "User",
    Item: {
      id: "5678",
      firstname: "para",
      lastname: "mesh"
    }
  }

  try {
    const data = await documentClient.put(params).promise();
    console.log(data);
  } catch (err) {
    console.log(err);
  }
}